/**
 * Created by ismael on 28/11/2016.
 */
var Layers = function () {
    return {
        first: {
            el:document.getElementById('canvas'),
            ct: null
        },
        second: {
            el:document.getElementById('canvas2'),
            ct: null
        },
        third: {
            el:document.getElementById('canvas3'),
            ct: null
        },
        fourth: {
            el:document.getElementById('canvas4'),
            ct: null
        },
        fifth: {
            el:document.getElementById('canvas5'),
            ct: null
        },
        sixth: {
            el:document.getElementById('canvas6'),
            ct: null
        },
        seventh: {
            el:document.getElementById('canvas7'),
            ct: null
        }
    }
};

module.exports = Layers;