/**
 * Created by ismael on 26/11/2016.
 */
class Donut {
    constructor(minR,sliceStroke,isFull=true) {
        // The offset from the center is the parameter minR
        this.minR = minR;
        // The "donut" size is this parameter, basically he'll be used as an offset of the high stroked circle.
        this.sliceStroke = sliceStroke;
        if(isFull) {
            this.type = "DonutSlice";
        } else {
            this.type = "PartialCircleDonut";
        }
    }
    getSliceOffset() {
        return this.minR+this.sliceStroke/2;
    }
    getSliceStroke() {
        return this.getSliceOffset()+this.sliceStroke/2+1;
    }
}
module.exports = Donut;