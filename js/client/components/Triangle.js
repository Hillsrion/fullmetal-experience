/**
 * Created by ismael on 23/11/2016.
 */
class Triangle {
    constructor(a,b,c,lineWidth,strokeColor, fill, fillColor) {
        this.a = a;
        this.b = b;
        this.c = c;
        this.points = [a,b,c];
        this.lineWidth = lineWidth;
        this.strokeColor = strokeColor;
        this.fillColor = fillColor;
        this.fill = fill;
        this.type = "Triangle";
    }
    getMiddlePoints() {
        const a = this._getMiddlePoint(this.a,this.b);
        const b = this._getMiddlePoint(this.b,this.c);
        const c = this._getMiddlePoint(this.c,this.a);
        return {a,b,c};
    }
    _getMiddlePoint(a,b) {
        let x,y;
        if(a.x==b.x) {
            x = a.x;
        } else {
            x = a.x + (b.x - a.x)/2;
        }
        if(a.y==b.y) {
            y = a.y;
        } else {
            y = a.y + (b.y - a.y)/2;
        }
        return {x,y}
    }
    render(ctx) {
        // console.log(this.a,this.b,this.c);
        ctx.beginPath();
        ctx.lineWidth = this.lineWidth;
        ctx.strokeStyle = this.strokeColor;
        ctx.moveTo(this.a.x+2,this.a.y);
        ctx.lineTo(this.b.x,this.b.y-2);
        ctx.lineTo(this.c.x-2,this.c.y);
        ctx.lineTo(this.a.x+2,this.a.y);
        ctx.lineJoin = 'bevel';
        if(!this.fill) {
            ctx.strokeStyle = this.strokeColor;
            ctx.stroke();
        } else {
            ctx.fillStyle = this.fillColor;
            ctx.fill();
        }
        ctx.closePath();
    }
}

module.exports = Triangle;