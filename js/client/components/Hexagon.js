/**
 * Created by ismael on 23/11/2016.
 */
class Hexagon {
    constructor(size,x,y,color,lineWidth,animated=false,fill=false) {
        this.sidesNumber = 6;
        this.size = size;
        this.x = x;
        this.y = y;
        this.color = color;
        this.lineWidth = lineWidth;
        // More perf if the allocate from the beginning the desired slots;
        this.points = new Array(this.sidesNumber);
        this.animated = animated;
        if(this.animated) {
            this.nextPoints = new Array(this.sidesNumber);
        }
        this.type = "Hexagon";
        this.fill = fill;
        this.draw();
    }
    draw() {
        let i=1;
        let sideLen = this.sidesNumber;
        let x,y,thisSlice,point;
        for(i;i<=sideLen;i++) {
            /**
             * We divide the circle in 6 parts
             * and we start from the top until get in the last iterations negative angle to finish the hexagon.
             * A bit tricky, but it works
             */
            thisSlice = (sideLen-1.5-i) * 2 * Math.PI / sideLen;
            x = this.x + this.size* Math.cos(thisSlice);
            y = this.y + this.size * Math.sin(thisSlice);
            point = {x,y};
            this.store(point,i-1);
        }
        // console.log(this.points);
    }
    render(ctx) {
        let i=0;
        let len = this.points.length;
        let x,y;
        ctx.beginPath();
        ctx.strokeStyle = this.color;
        ctx.lineWidth = this.lineWidth;
        ctx.moveTo(this.x ,this.y - this.size);
        for(i;i<len;i++) {
            x = this.points[i].x;
            y = this.points[i].y;
            ctx.lineTo(x,y);
        }
        if(this.fill) {
            ctx.fillStyle = this.fillColor;
            ctx.fill();
        } else {
            ctx.strokeStyle = this.strokeColor;
            ctx.stroke();
        }
        ctx.closePath();
    }
    getMiddlePoints() {
        var arr = [];
        let i = 0;
        let len = this.points.length;
        var point,nextPoint;
        for(i;i<len;i++) {
            point = this.points[i];
            if(i<len-1) {
                nextPoint = this.points[i+1];
                // console.log(point,nextPoint);
                // console.log(i,len);
                arr.push(this._getMiddlePoint(point,nextPoint))
            } else if(i==len-1) {
                arr.push(this._getMiddlePoint(this.points[len-1],this.points[0]));
            }
        }
        // console.log(arr);
        return arr;
    }
    getPreviousPoints() {
        var arr = [];
        let i = 0;
        let len = this.points.length;
        var point,nextPoint;
        for(i;i<len;i++) {
            point = this.points[i];
            if(i<len-1) {
                nextPoint = this.points[i+1];
                // console.log(point,nextPoint);
                // console.log(i,len);
                arr.push(this._getPreviousPoint(point,nextPoint))
            } else if(i==len-1) {
                arr.push(this._getPreviousPoint(this.points[len-1],this.points[0]));
            }
        }
        // console.log(arr);
        return arr;
    }
    _getMiddlePoint(a,b) {
        let x,y;
        if(a.x==b.x) {
            x = a.x;
        } else {
            x = a.x + (b.x - a.x)/2;
        }
        if(a.y==b.y) {
            y = a.y;
        } else {
            y = a.y + (b.y - a.y)/2;
        }
        return {x,y}
    }
    _getPreviousPoint(a,b) {
        let x,y;
        if(a.x==b.x) {
            x = a.x;
        } else {
            x = (b.x - a.x);
        }
        if(a.y==b.y) {
            y = a.y;
        } else {
            y = (b.y - a.y);
        }
        return {x,y}
    }
    store(point,index,field="points") {
        this.points[index] = point;
    }
}

module.exports = Hexagon;