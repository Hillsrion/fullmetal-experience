/**
 * Created by ismael on 25/11/2016.
 */
class Line {
    constructor(a,b,lineWidth,color) {
        this.a = a;
        this.b = b;
        this.color = color;
        this.type = "lines";
        this.lineWidth = lineWidth;
    }
    render(ctx) {
        ctx.beginPath();
        ctx.lineWidth = this.lineWidth;
        ctx.strokeStyle = this.color;
        ctx.moveTo(this.a.x,this.a.y);
        ctx.lineTo(this.b.x,this.b.y);
        ctx.stroke();
        ctx.closePath();
    }
}

module.exports = Line;
