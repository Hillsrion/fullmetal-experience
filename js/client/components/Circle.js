/**
 * Created by ismael on 23/11/2016.
 */
class Circle {
    constructor(x=0,y=0,r=15,strokeWidth,strokeColor,fillColor,fill=false,proportions) {
        this.x = x;
        this.y = y;
        this.r = r;
        this.strokeWidth = strokeWidth;
        this.type = "circles";
        this.fillColor = fillColor;
        this.fill = fill;
        this.strokeColor = strokeColor;
        if(!proportions || proportions==undefined) {
            this.proportions = this.getDefaultProportions();
        } else {
            this.proportions = proportions;
        }
        this.startAngle = this.proportions.start;
        this.endAngle = this.proportions.end;
    }
    getDefaultProportions() {
        return {
            start: 0,
            end: 2*Math.PI
        }
    }
    render(ctx) {
        ctx.beginPath();
        ctx.lineWidth = this.strokeWidth;
        ctx.arc(this.x,this.y,this.r,this.startAngle,this.endAngle,false);
        if(this.fill) {
            ctx.fillStyle = this.fillColor;
            ctx.fill();
        } else {
            ctx.strokeStyle = this.strokeColor;
            ctx.stroke();
        }
        ctx.closePath();
    }
}

module.exports = Circle;