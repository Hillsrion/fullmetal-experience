/**
 * Created by ismael on 18/11/2016.
 */
// Libs
var _ = require('../../node_modules/lodash/lodash');
var App = {
    layers: require('./helpers/Layers')(),
    helpers: {
        Donut: require('./helpers/Donut'),
        records: []
    },
    Circle: require('./components/Circle'),
    Hexagon: require('./components/Hexagon'),
    Triangle: require('./components/Triangle'),
    Line: require('./components/Line'),
    colors: {
        blue: "dodgerblue",
        white: "white",
        background: getComputedStyle(document.body,null).getPropertyValue('background-color'),
        default: 'white'
    },
    sentences: [
        "Pale black and white in a false vitrine",
        "Imperfect white and red.The peacock's",
        "feather in bright colors,the rainbow in the",
        "sky above, the spotted panther, the green",
        "lion,the crow's beak, blue as lead. They ",
        'shall appear before you in perfect white.'
    ],
    circleComponents: {
        circles: {
            regular: []
        },
        lines: [],
        hexagon: null,
        triangle: null
    },
    windowSettings: {
        height: window.innerHeight,
        width: window.innerWidth
    },
    componentsSettings: {
        masterCircle: 240,
        partialCircle: 70,
        fullAngle: Math.PI*2,
        lines: [
            {start:1,end:4},
            {start:2,end:5},
            {start:6,end:3}
        ],
        hexagonMainPoints:{a:0, b:2, c:4},
        lineWidth: {
            regular: 3,
            medium: 2.5,
            thin: 2
        }
    },
    tl: new TimelineMax(),
    drawTl: new TimelineMax(),
    homeTl: new TimelineMax,
    interactionTl: new TimelineMax(),
    debugMode: false,
    init: function () {
        if(this.debugMode) {
            this.debugLayer = this.layers.seventh;
            this.debugLayer.el.addEventListener('mousemove', this.getMousePos.bind(this));
        }
        this.settleCanvas();
        this.tl.pause();
        this.drawTl.pause();
        this.homeTl.pause();
        this.interactionTl.pause();
        this.initHome();
        this.draw();
        let toggle = document.querySelector('.toggle');
        toggle.addEventListener('click',this.toggleVideo.bind(this))
    },
    toggleVideo: function () {
        let iframe = document.createElement('iframe');
        iframe.setAttribute("src","https://www.youtube.com/embed/ZLQL1-XhQ1M?autoplay=1&controls=0&showinfo=0&iv_load_policy=3&rel=0&start=40");
        iframe.setAttribute('frameborder',"0");
        iframe.setAttribute('allowfullscreen',true);
        document.body.appendChild(iframe);
        iframe.classList.add('reveal');
        iframe.classList.add('outro');
        TweenMax.to(iframe,0.7,{x:0,force3D:true,delay:0.5,ease:Power3.easeInOut});
        let audio = document.querySelector('audio');
        audio.pause();
    },
    initHome: function () {
        let title = document.querySelector('.title > span');
        let button = document.querySelector('.button');
        let video = document.querySelector('.intro-video');
        console.log(title,button);
        TweenMax.set([button,video],{autoAlpha:0});
        this.tl.play();
        this.tl.add(this.homeTl);
        this.homeTl.play();
        this.homeTl.add(TweenMax.to(title,1.2,{y:0,delay:0.75,force3D:true, ease:Power4.easeInOut}));
        this.homeTl.add(TweenMax.to(video,.5,{autoAlpha:1, ease:Power2.easeOut, onComplete: function () {
            let audio = document.querySelector('audio');
            audio.play();
        }}));
        this.homeTl.add(TweenMax.to(button,.3,{autoAlpha:1, ease:Power2.easeOut,onComplete:(function () {
            this.tl.pause();
        }).bind(this)}));
        button.addEventListener('click',this.getNext.bind(this))
    },
    getNext: function () {
        // console.log('click');
        let panel = document.querySelector(".transition-panel");
        let home = document.querySelector(".home");
        let canvasList = document.querySelector(".canvas-list");
        this.canvasList = canvasList;
        this.tl.play();
        this.tl.add(TweenMax.to(panel,0.65,{x:0,force3D:true,ease:Circ.easeOut,onComplete: function () {
            TweenMax.set(canvasList,{x:0,force3D:true});
            TweenMax.set(home, {x:-home.offsetWidth*2,force3D:true});
        }}));
        this.tl.add(TweenMax.to(panel,0.8,{x:-panel.offsetWidth,force3D:true,ease:Circ.easeInOut,onComplete: (function () {
            this.tl.pause();
            this.render();
        }).bind(this)}));
    },
    settleCanvas: function () {
        let layer;
        for(layer in this.layers) {
            // Setting size attributes.
            this.layers[layer].el.setAttribute("height",this.windowSettings.height);
            this.layers[layer].el.setAttribute("width",this.windowSettings.width);
            // Getting canvas layer.
            this.layers[layer].ct = this.layers[layer].el.getContext('2d');
        }
    },
    settleIcons: function () {
        let x,y;
        let i = 1;
        var list = document.querySelector('.miniCircle');
        var el;
        _.forEach(this.circleComponents.circles.miniCircleFilled,function (point) {
            x = point.x;
            y = point.y;
            el = list.querySelector('[data-index="'+i+'"]');
            el.style.left = x+'px';
            el.style.top = y+'px';
            TweenMax.to(el,.45,{scale:1,autoAlpha:1,ease:Power3.easeOut});
            i++;
        });
    },
    draw: function () {
        // Settings.
        const x = this.windowSettings.width/2;
        const y = this.windowSettings.height/2;
        const firstLayer = this.layers.first;
        const secondLayer = this.layers.second;
        const thirdLayer = this.layers.third;
        const fourthLayer = this.layers.fourth;
        const regular = this.componentsSettings.lineWidth.regular;
        const medium = this.componentsSettings.lineWidth.medium;
        const strokeColor = this.colors.default;
        const emptyAngles = {start:0,end:0};
        // 240
        const baseR = this.componentsSettings.masterCircle;
        // 185
        const subCircle = baseR - 40;
        const textSpace = baseR - 28;
        // For hexagon build.
        this.componentsSettings.subCircle = subCircle;
        // Drawing main circle.
        this.makeCircle(firstLayer,x,y,baseR,regular,strokeColor,"mainCircles",'',false,emptyAngles);
        // Drawing stroke main circle
        this.makeCircle(firstLayer,x,y,baseR-10,regular,strokeColor,"mainCircles",'',false,emptyAngles);
        // Drawing sub-circle
        this.makeCircle(firstLayer,x,y,subCircle,medium,strokeColor,"mainCircles",'',false,emptyAngles);
        this.makeHexagon(true);
        // // Drawing Hexagon circle
        this.makeCircle(firstLayer,x,y,subCircle-28,2,strokeColor,"hexagonCircle",'',false,emptyAngles);
        this.makeLines();
        this.makeTriangle();
        // Near center
        this.makeDonut(x,y,42,10,this.colors.default,firstLayer,'centerDonut',{start:0,end:Math.PI*2});
        // One-third-circles;
        this.drawPartialCircles();
        // Mini-circles around triangle
        this.drawMiniCircles();
        this.drawBendedText(this.layers.third.ct,this._displayText(this.sentences),x,y,textSpace,Math.PI*2);
        this.get('Hexagon').render(fourthLayer.ct);
        this.get('Triangle').render(this.layers.sixth.ct);
        // this.render();
    },
    render: function () {
        let fullAngle = this.componentsSettings.fullAngle;
        const firstLayer = this.layers.first;
        const secondLayer = this.layers.second;
        const thirdLayer = this.layers.third;
        const fourthLayer = this.layers.fourth;
        const fifthLayer = this.layers.fifth;
        const sixthLayer = this.layers.sixth;
        let mainCircles = this.get('circles','mainCircles');
        let hexagonCircle = this.get('circles','hexagonCircle');
        let centerCircle = this.get('circles','centerDonut');
        let miniCircles = this.get('circles','miniCircle').concat(this.get("circles","miniCircleFilled"));
        let partialCircles = this.get('circles',"partialCircles");
        let lines = this.circleComponents.lines;
        let rotationTweens = [
            TweenMax.to(fourthLayer.el,0.7,{rotation:0, autoAlpha:1,ease:Power3.easeInOut}),
            TweenMax.to(thirdLayer.el,0.7,{rotation:10, autoAlpha:1,ease:Power3.easeInOut})
        ];
        this.tl.play(this.drawTl);
        this.drawTl.play();
        this.drawTl.add(
            TweenMax.staggerTo(mainCircles,1.15,{
                endAngle: fullAngle,
                delay:0.5,
                ease: Power2.easeInOut,
                onUpdate: this.update.bind(this,mainCircles,firstLayer.ct)
            },0.17));
        this.drawTl.add(rotationTweens);
        this.drawTl.add(
            TweenMax.to(hexagonCircle,1.15,{
                endAngle: fullAngle,
                ease: Power2.easeInOut,
                onUpdate: this.update.bind(this,hexagonCircle,firstLayer.ct)
            }));
        let layers = [firstLayer,secondLayer,fifthLayer];
        for(let i=0;i<lines.length;i++) {
            let xDel = lines[i].b.x;
            let yDel = lines[i].b.y;
            lines[i].b.x = lines[i].a.x;
            lines[i].b.y = lines[i].a.y;
            this.drawTl.add(
                TweenMax.to(lines[i].b,0.3,{
                    x:xDel,
                    y:yDel,
                    onUpdate: function () {
                        lines[i].render(layers[i].ct);
                    }
                })
            );
        }
        this.drawTl.add(
            TweenMax.fromTo(centerCircle.concat(miniCircles),1,{endAngle:0},{
                endAngle: fullAngle,
                ease: Power2.easeInOut,
                onUpdate: this.update.bind(this,centerCircle.concat(miniCircles),this.layers.seventh.ct),
                onComplete: this.settleIcons.bind(this)
            }));
        this.drawTl.add(this.getPartialCirclesTweens());
        this.drawTl.add(TweenMax.to(sixthLayer.el,0.4,{scale:1,autoAlpha:1,ease:Power4.easeOut,force3D:true,onComplete:(function () {
            // this.tl.pause();
            // this.initInteraction();
        }).bind(this)}));
        let items = [
            TweenMax.to(this.canvasList,1.2,{x:-300,force3D:true,ease:Power4.easeInOut}),
            TweenMax.to(document.querySelectorAll('svg[data-owner]'),1.2,{left:'-=300px',ease:Power4.easeInOut})
        ];
        this.drawTl.add(items);
        let text = document.querySelectorAll('.presentation p,.presentation div');
        console.log(text);
        this.drawTl.add(TweenMax.staggerTo(text,0.35,{x:0,autoAlpha:1,force3D:true,ease:Power2.easeInOut},0.15));
    },
    initInteraction: function () {
        // this.tl.play();
        // this.tl.add(this.interactionTl);
        let canvasList = document.querySelector(".canvas-list");
        // this.tl.add();
    },
    update: function (el,ctx) {
        const element = arguments[0];
        const context = arguments[1];
        if(element.length > 1) {
            let i = 0;
            let len = element.length;
            _.forEach(element, function (el) {
                el.render(context);
            })
        } else {
            element.render(context);
        }
    },
    get: function (type,category) {
        let el;
        if(!category) {
            el = _.find(this.circleComponents,{type:type});
            if(el.length==0) {
                el = this.circleComponents[type];
            }
        } else {
            el = this.circleComponents[type][category];
            if(el.length==1) {
                el = el[0];
            }
        }
        return el;
    },
    getPartialCirclesTweens: function () {
        let result = [];
        let partialCircles = this.get('circles',"partialCircles");
        var tween,save;
        _.forEach(partialCircles, (function (circle) {
            save = circle.endAngle;
            circle.endAngle = circle.startAngle;
            // console.log(circle.endAngle,save);
            tween = TweenMax.to(circle,1,
                {
                endAngle: save,
                ease: Power2.easeInOut,
                onUpdate: this.update.bind(this,circle,this.layers.fifth.ct)
                });
            // console.log(circle);
            result.push(tween);
        }).bind(this));
        return result;
    },
    _displayText : function (sentences) {
        let i = 0;
        let result = '';
        let len = sentences.length;
        for(i;i <len;i++) {
            result = result + sentences[i]+'                ';
        }
        // console.log(result);
        return result;
    },
    makeCircle: function (layer,x,y,r,lineWidth=this.componentsSettings.lineWidth.regular, strokeColor=this.colors.default,category=false,fillColor=false,fill=false,angles) {
        const circle = new this.Circle(x,y,r,lineWidth,strokeColor,fillColor,fill,angles);
        if(category) {
            circle.category = category;
            if(!this.circleComponents.circles[category]) {
                this.circleComponents.circles[category] = [circle];
            } else {
                this.circleComponents.circles[category].push(circle);
            }
        } else {
            this.circleComponents.circles.regular.push(circle);
        }
        return circle;
    },
    drawBendedText: function (ctx, str, x, y, radius, angle) {
        let len = str.length;
        let i = 0;
        var letter;
        ctx.save();
        ctx.translate(x, y);
        ctx.font = "9pt Blacksword";
        ctx.fillStyle = this.colors.white;
        ctx.strokeStyle = this.colors.white;
        for (i; i < len; i++) {
            ctx.rotate(angle/len);
            ctx.save();
            ctx.translate(0, -1 * radius);
            letter = str[i];
            ctx.fillText(letter, 0, 0);
            ctx.restore();
        }
        ctx.restore();
        this.circleComponents.texts = this.circleComponents.texts || [];
        this.circleComponents.texts.push({text:str,x,y,radius,angle});
    },
    makeDonut: function (x,y,size,slice,isFull,layer=this.layers.second,category,angles={}) {
        // console.log(angles);
        const donut = new this.helpers.Donut(size,slice,isFull);
        const defaultLineWidth = this.componentsSettings.lineWidth.thin;
        this.makeCircle(layer,x,y,donut.getSliceStroke(),defaultLineWidth,this.colors.default,category,'',false,angles);
        this.makeCircle(layer,x,y,donut.getSliceOffset(donut.minR),donut.sliceStroke,this.colors.background,category,'',false,angles);
        this.makeCircle(layer,x,y,donut.minR,defaultLineWidth,this.colors.default,category,'',false,angles);
        this.helpers.records.push(donut);
    },
    makeHexagon: function (isAnimated) {
        const size = this.componentsSettings.subCircle-1;
        const hexagon = new this.Hexagon(size,this.windowSettings.width/2,this.windowSettings.height/2,this.colors.default,2,isAnimated);
        this.circleComponents.hexagon = hexagon;
        return hexagon;
    },
    makeLines: function () {
        let i = 0;
        const lineWidth = this.componentsSettings.lineWidth.thin;
        const lines = this.componentsSettings.lines;
        const hexagon = this.circleComponents.hexagon;
        let len = lines.length;
        let line,x,y,a,b;
        for(i;i<len;i++) {
            a = lines[i].start;
            b = lines[i].end;
            // Need a 0 based indexation to get the right points.
            x = hexagon.points[a-1];
            y = hexagon.points[b-1];
            line = new this.Line(x,y,lineWidth,this.colors.default);
            this.circleComponents.lines.push(line);
        }
    },
    drawMiniCircles: function (category) {
        const triangle = this.circleComponents.triangle;
        const points = triangle.getMiddlePoints();
        const donut = _.find(this.helpers.records,{type:"DonutSlice"});
        const r = donut.minR/1.5;
        const lineWidth = this.componentsSettings.lineWidth.regular;
        const r2 = r-lineWidth+1.5;
        // Can't use a let there, we got an other block
        var layer;
        _.forEach(points,(function (point) {
            // console.log(point);
            // console.log(donut);
            layer = this.layers.second;
            this.makeCircle(layer,point.x,point.y,r,lineWidth,this.colors.default,"miniCircle",'',false);
            this.makeCircle(layer,point.x,point.y,r2,lineWidth,"","miniCircleFilled",this.colors.background,true);
        }).bind(this));
    },
    drawPartialCircles: function() {
        const triangle = this.circleComponents.triangle;
        let i = 1;
        const layer = this.layers.second;
        var angles;
        // console.log(triangle);
        _.forEach(triangle.points,(function (point) {
            angles = this._getAngles(i);
            this.makeDonut(point.x,point.y,this.componentsSettings.partialCircle,18,false,layer,"partialCircles",angles);
            i++;
        }).bind(this));
    },
    _getAngles: function (index) {
        const angles = {};
        if(index==1) {
            /* General notice : To get the base where i'll remove 60deg to have the start and add 60 for the end
             * i'll always need a base angle (the top of the circle if he would be vertical).
             * This base angle is showed graphically by the line starting from triangle base point.
             */
            /**
             * There the line starts a 30deg== 2pi/12 on the circle and i know it because the line
             * cut the equilateral triangle's angle in half so i know this is 30deg
             */
            angles.start = 2*Math.PI/12 - this._degreesToRadians(60);
            angles.end = 2*Math.PI/12 + this._degreesToRadians(60);
        } else if(index==2) {
            /**
             * We know the angle of the hexagon side(120)
             * So if we start at 3/4 of the circle (in a clocwise direction); So to speak, the top.
             * We can have the start by removing the half of the angle and adding 3/4PI/2
             * And the end by the same process but adding the degrees.
             */
            angles.start = (2*Math.PI/4)*3 - this._degreesToRadians(60);
            angles.end = (2*Math.PI/4)*3 + this._degreesToRadians(60);
        } else if(index==3) {
            /**
             * I move at left half of the circle
             * Then i remove 30deg, or PI/6, 30 deg because i know that the line i 30deg below
             * Because it curts the triangle equilatetal angle(60deg) in half.
             */
            angles.start = Math.PI - Math.PI/6 - this._degreesToRadians(60);
            angles.end = Math.PI - Math.PI/6 + this._degreesToRadians(60);
        }
        return angles;
    },
    _degreesToRadians: function (degrees,inverse=false) {
        if(!inverse) {
            return degrees*(Math.PI/180);
        } else {
            return degrees*(180/Math.PI);
        }
    },
    makeTriangle: function () {
        const lineWidth = this.componentsSettings.lineWidth.regular;
        const points = this.getHexagonMainPoints();
        const triangle = new this.Triangle(points.a,points.b,points.c,lineWidth,this.colors.default);
        this.circleComponents.triangle = triangle;
    },
    getHexagonMainPoints: function(){
        /**
         * This parameters are integers, matching the desired points of the hexagon
         * The points saved in the array starts to be saved at the end of the first lineTo.
         * Thats means that starting from the top in a anticlockwise direction, point index 0 is the 2nd point
         * Because i need a 0 based indexation.
         */
        const hexagon = this.circleComponents.hexagon;
        // 0
        const aIndex = this.componentsSettings.hexagonMainPoints.a;
        // 2
        const bIndex = this.componentsSettings.hexagonMainPoints.b;
        // 4
        const cIndex = this.componentsSettings.hexagonMainPoints.c;
        const a = hexagon.points[aIndex];
        const b = hexagon.points[bIndex];
        const c = hexagon.points[cIndex];
        return {a,b,c};
    },
    getMousePos: function (ev) {
        let rect = this.debugLayer.el.getBoundingClientRect();
        let x = ev.clientX - rect.left;
        let y = ev.clientY - rect.top;
        if(!this.hasDebugEl) {
            this.debugEl = document.createElement('h1');
            this.debugEl.classList.add('debug');
            document.body.appendChild(this.debugEl);
            this.debugEl.textContent = 'Mouse position :'+x+','+y;
            this.hasDebugEl = true;
        } else {
            this.debugEl.textContent = 'Mouse position :'+x+','+y;
        }
    }
};

App.init();

console.log(App);