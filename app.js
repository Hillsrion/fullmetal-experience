/**
 * Created by ismael on 17/11/2016.
 */
var App = {
    express: require('express'),
    port:4000,
    init: function () {
        // Initializing server and websocket connexion.
        this.app = this.express();
        this.server = require('http').createServer(this.app);
        this.io = require('socket.io').listen(this.server);
        // Middlewares serving files.
        this.app.use(this.express.static('js'));
        this.app.use(this.express.static('css'));
        this.app.use(this.express.static('font'));
        this.app.use(this.express.static('videos'));
        // Routing index.
        this.app.get('/',function (req,res) {
            res.sendFile('index.html',{root:__dirname+'/views'});
        });
        // Success launch.
        this.server.listen(this.port,(function () {
            console.log('Serveur lancé sur le port '+this.port);
        }).bind(this))
    }
};

App.init();