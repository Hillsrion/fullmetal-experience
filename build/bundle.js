(function e(t,n,r){function s(o,u){if(!n[o]){if(!t[o]){var a=typeof require=="function"&&require;if(!u&&a)return a(o,!0);if(i)return i(o,!0);var f=new Error("Cannot find module '"+o+"'");throw f.code="MODULE_NOT_FOUND",f}var l=n[o]={exports:{}};t[o][0].call(l.exports,function(e){var n=t[o][1][e];return s(n?n:e)},l,l.exports,e,t,n,r)}return n[o].exports}var i=typeof require=="function"&&require;for(var o=0;o<r.length;o++)s(r[o]);return s})({1:[function(require,module,exports){
/**
 * Created by ismael on 18/11/2016.
 */
var test = require('./test');
test();
console.log('ok');
var App = {
    canvas: document.getElementById('canvas'),
    windowSettings: {
        height: window.innerHeight,
        width: window.innerWidth
    },
    init: function (){
        console.log("hello");
        this.settleCanvas();
        this.drawCircle();
    },
    settleCanvas: function () {
        this.canvas.setAttribute("height",this.windowSettings.height);
        this.canvas.setAttribute("width",this.windowSettings.width);
        this.ct = this.canvas.getContext('2d');
    },
    drawCircle: function () {
        var x = this.windowSettings.width;
        var y = this.windowSettings.height;
    }
};

App.init();

class Circle {
    constructor(x,y,r,color,strokeWidth) {
        this.x = x;
        this.y = y;
        this.r = r;
        this.color = color;
        this.strokeWidth = strokeWidth;
    }
    render(ctx) {
        ctx.beginPath();
        ctx.strokeStyle = this.color;
        ctx.arc(this.x/2,this.y/2,this.r,0,Math.PI*360/180);
        ctx.stroke();
        ctx.closePath();
    }
}
},{"./test":2}],2:[function(require,module,exports){
/**
 * Created by ismael on 22/11/2016.
 */
module.exports = function () {
    console.log("tamer");
}
},{}]},{},[1]);
