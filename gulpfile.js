var gulp = require('gulp');
var sass = require('gulp-sass');
var cleanCSS = require('gulp-clean-css');
var rename = require("gulp-rename");
var uglify = require('gulp-uglify');
var streamify = require('streamify');
var nodemon = require('gulp-nodemon');
var babel = require('gulp-babel');
var browserify = require('browserify');
var source = require('vinyl-source-stream');
var buffer = require('vinyl-buffer');

gulp.task('start', function () {
    nodemon({
        script: 'app.js',
        ext: 'js html',
        env: { 'NODE_ENV': 'development' }
    })
});


// Compile sass files from /scss into /css
gulp.task('sass', function() {
    return gulp.src('scss/app.scss')
        .pipe(sass())
        .pipe(gulp.dest('css'))
});

// Minify compiled CSS
gulp.task('minify-css', ['sass'], function() {
    return gulp.src('css/app.css')
        .pipe(cleanCSS({ compatibility: 'ie8' }))
        .pipe(rename({ suffix: '.min' }))
        .pipe(gulp.dest('css'))
});

// Minify JS
gulp.task('minify-js', function() {
        browserify('js/client/app.js')
        .bundle()
        .pipe(source('bundle.js'))
        .pipe(buffer())
        .pipe(babel({
            presets: ['es2015']
        }))
        .pipe(rename({basename:'app' }))
        .pipe(gulp.dest('js/client/build'))
        .pipe(rename({ suffix: '.min'}))
        .pipe(gulp.dest('js/client/build'))
});


// Run everything
gulp.task('default', ['sass', 'minify-css', 'minify-js','start']);

// Using nodemon on task dev
gulp.task('dev', ['sass', 'minify-css', 'minify-js','start'], function() {
    gulp.watch('scss/*.scss', ['sass']);
    gulp.watch('css/*.css', ['minify-css']);
    gulp.watch('js/client/*.js', ['minify-js']);
    gulp.watch('js/client/components/*.js', ['minify-js']);
});
